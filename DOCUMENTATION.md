# Open Source License API

[Download Swagger specification](http://licenses.humboldtapps.com/spec.yaml)

This API is for developers that want to add a plaintext license to their project but don't want to manually search the web for a copy.

All API requests are ``GET`` requests and all responses are strings to make consuming the API via terminal or console simple.
  
This API and its source code are licensed under the [MIT License](http://licenses.humboldtapps.com/LICENSE.txt).
  
## Table of Contents (Ctrl+F to jump to section)
* List
  * List all available endpoints
  * List IDs of all available licenses
  * List IDs and names of all available licenses
* License
  * Retrieve a license using its ID
  
---
  
## List
  
### List all available endpoints
Returns a list of available endpoints.
  
#### Usage
cURL
  
```
curl http://licenses.humboldtapps.com/api/
```
  
PowerShell
  
```
Invoke-WebRequest 'http://licenses.humboldtapps.com/api/' | Select-Object -Expand Content
```  
### Responses
#### 200 Successful operation
Example:
  
```
===============================================================
                      Available Endpoints                      
===============================================================
   Endpoint: /api/
Description: List all available endpoints
-
   Endpoint: /api/list
Description: List all available open source licenses by id
===============================================================
```
  
#### 503 Service unavailable
Example:
  
```
ERROR - Endpoint list not found! Contact the site administrator if the problem persists.
```
  
---

### List IDs of all available licenses
Returns a list of all available license IDs. Limited to **5** licenses per row for readability.
  
#### Usage
cURL
  
```
curl http://licenses.humboldtapps.com/api/list
```
  
PowerShell
  
```
Invoke-WebRequest 'http://licenses.humboldtapps.com/api/list' | Select-Object -Expand Content
```
### Responses
#### 200 Successful operation
Example:
  
```
gpl-2.0, gpl-3.0, isc
```
  
#### 503 Service unavailable
Example:
  
```
ERROR - License database not found! Contact the site administrator if the problem persists.
```
  
---
  
### List IDs and names of all available licenses
Returns a detailed list of available licenses.
  
#### Usage
cURL
  
```
curl http://licenses.humboldtapps.com/api/list/full
```
  
PowerShell
  
```
Invoke-WebRequest 'http://licenses.humboldtapps.com/api/list/full' | Select-Object -Expand Content
```  
### Responses
#### 200 Successful operation
Example:
  
```
gpl-2.0             GNU General Public License v2.0
gpl-3.0             GNU General Public License v3.0
isc          
```
  
#### 503 Service unavailable
Example:
  
```
ERROR - License database not found! Contact the site administrator if the problem persists.
```
  
---
  
## License
  
### Retrieve a license using its ID
Returns a single license using its ID.

| parameter | type | description | required |
|-----------|------|-------------|----------|
| license | string | The name of the license to retrieve. | yes |
  
#### Usage
cURL
  
```
curl http://licenses.humboldtapps.com/api/{license}
```
  
PowerShell
  
```
Invoke-WebRequest 'http://licenses.humboldtapps.com/api/{license}' | Select-Object -Expand Content
```    
### Responses
#### 200 Successful operation
Example:
  
```
ISC License

Copyright (c) [year], [fullname]

Permission to use, copy, modify, and/or distribute this software
for any purpose with or without fee is hereby granted, provided
that the above copyright notice and this permission notice appear
in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
```
  
#### 404 Not found
Example:
  
```
ERROR - NonExistentLicense is undefined. Use the 'list' endpoint to see defined licenses.
```
