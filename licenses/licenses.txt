afl-3.0             Academic Free License v3.0
agpl-3.0            GNU Affero General Public License v3.0
apache-2.0          Apache License 2.0
artistic-2.0        Artistic License 2.0
bsd-2-clause        BSD 2-Clause "Simplified" License
bsd-3-clause-clear  BSD 3-Clause Clear License
bsd-3-clause        BSD 3-Clause "New" or "Revised" License
bsl-1.0             Boost Software License 1.0
cc-by-4.0           Creative Commons Attribution 4.0 International
cc-by-sa-4.0        Creative Commons Attribution Share Alike 4.0 International
cc0-1.0             Creative Commons Zero v1.0 Universal
ecl-2.0             Educational Community License v2.0
epl-1.0             Eclipse Public License 1.0
epl-2.0             Eclipse Public License 2.0
eupl-1.1            European Union Public License 1.1
eupl-1.2            European Union Public License 1.2
gpl-2.0             GNU General Public License v2.0
gpl-3.0             GNU General Public License v3.0
isc                 ISC License
lgpl-2.1            GNU Lesser General Public License v2.1
lgpl-3.0            GNU Lesser General Public License v3.0
lppl-1.3c           LaTeX Project Public License v1.3c
mit                 MIT License
mpl-2.0             Mozilla Public License 2.0
ms-pl               Microsoft Public License
ms-rl               Microsoft Reciprocal License
ncsa                University of Illinois/NCSA Open Source License
ofl-1.1             SIL Open Font License 1.1
osl-3.0             Open Software License 3.0
postgresql          PostgreSQL License
unlicense           The Unlicense
upl-1.0             Universal Permissive License v1.0
wtfpl               Do What The F*ck You Want To Public License
zlib                zlib License
